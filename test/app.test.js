'use strict';

const assert = require('chai').assert;

const { handler } = require('../app');

describe('test 200', function () {
  it('should upload to s3', async function () {
    const result = await handler();

    assert(result.statusCode, 200);
  });
});
