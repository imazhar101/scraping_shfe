'use strict';

const { getFuturesData } = require('./src/futures/futures');
const { getAggregateReferenceDataFutures } = require('./src/futures/aggregate');
const { getOptionsData } = require('./src/options/options');
const { getAggregateReferenceDataOptions } = require('./src/options/aggregate');

const day = new Date().toISOString().slice(0, 10).replace(/-/g, '');
const dir = `${new Date().toISOString().slice(0, 10)}`;
const dateStr = `${new Date().toISOString().slice(0, 10).replace(/[-]/g, '')}`;

exports.handler = async (e) => {
  // Getting EOD price from SHFE exchange

  try {
    await getFuturesData(day, dir, dateStr);
    await getAggregateReferenceDataFutures(day, dir, dateStr);
    await getOptionsData(day, dir, dateStr);
    await getAggregateReferenceDataOptions(day,dir,dateStr)

    return {
      statusCode: 200
    };
  } catch (error) {
    console.error(`handler failed with ${error.stack}`);

    return {
      statusCode: 404
    };
  }
};
