'use strict';

require('dotenv').config();

const Papa = require('papaparse');
const axios = require('axios');

// aws-sdk is always preinstalled in AWS Lambda in all Node.js runtimes
const AWS = require('aws-sdk');
const s3 = new AWS.S3({ region: 'us-east-2' });

const dailyData = async function (day, dir, dateStr) {
  console.log(`running dailyData`);

  const { data } = await axios(
    `http://www.shfe.com.cn/data/dailydata/option/kx/kx${day}.dat` // url contains date in YYYYMMDD format
  );

  // const note =
  //   'Note： 1. Price quotation = RMB yuan/bbl for crude oil. RMB yuan/metric ton for Copper，Aluminum，Zinc，Natural Rubber. RMB yuan/g for Gold. 2. Contract size = 1000 bbl/lot for crude oil. 5 metric tons/lot for Copper，Aluminum，Zinc. 10 metric tons/lot for Natural Rubber. 1000 grams/lot for Gold. 3. Trading volume， open interest， and change in open interest are displayed in lots on one-sided basis (buy and sell in aggregate) turnover is measured on one-sided basis. 4. Ch1 = closing price - previous settlement price Ch2 = settlement price - previous settlement price. 5. Contract series: refers to all options contracts whose underlying futures contract is of the same delivery month. 6. Implied volatility: the price volatility of an option’s underlying futures contract calculated by entering the market price of the option into an option pricing model. 7. Options trading volume and turnover including that of self-trades. ';

  const regex1 = new RegExp(/总计/);
  const regex2 = new RegExp(/小计/);

  //get o_curinstrument data from API
  const currentInstrument = data.o_curinstrument.map((item) => {
    return {
      'Product ID': item.CLOSEPRICE ? item.PRODUCTID.trim() : '',
      'Product Group ID': item.CLOSEPRICE ? item.PRODUCTGROUPID.trim() : '',
      'Contract Code': regex2.test(item.INSTRUMENTID.trim())
        ? 'Subtotal:'
        : regex1.test(item.PRODUCTID.trim())
        ? 'Total:'
        : item.INSTRUMENTID.trim(),
      Open: item.OPENPRICE,
      High: item.HIGHESTPRICE,
      Low: item.LOWESTPRICE,
      Close: item.CLOSEPRICE,
      'Pre settle': item.PRESETTLEMENTPRICE,
      Settle: item.SETTLEMENTPRICE,
      ch1: item.ZD1_CHG,
      ch2: item.ZD2_CHG,
      Volume: item.VOLUME === '' ? 0 : item.VOLUME,
      'Open Interest': item.OPENINTEREST,
      'Open Interest Change': item.OPENINTERESTCHG,
      Turnover: Number(item.TURNOVER).toFixed(2),
      Delta: Number(item.DELTA).toFixed(2),
      'Exercise Vol': item.EXECVOLUME
    };
  });

  const removeByAttr = function (arr, attr, value) {
    var i = arr.length;
    while (i--) {
      if (arr[i] && arr[i].hasOwnProperty(attr) && arguments.length > 2 && arr[i][attr] === value) {
        arr.splice(i, 1);
      }
    }
    return arr;
  };

  removeByAttr(currentInstrument, 'Contract Code', 'Subtotal:');
  removeByAttr(currentInstrument, 'Contract Code', 'Total:');

  const s3Params = {
    Bucket: process.env.AWS_S3_BUCKET,
    Key: `${dir}/SHFE-EODPrice-Options-${dateStr}.csv`,
    Body: Papa.unparse(currentInstrument) //+ '\n' + note
  };

  await s3.putObject(s3Params).promise();

  console.log(`Completed uploading to s3 dailyData`);
};

const contractParameters = async function (day, dir, dateStr) {
  console.log(`running contractParameters`);

  const { data } = await axios.get(`http://www.shfe.com.cn/data/instrument/option/ContractBaseInfo${day}.dat`);

  // Choose the required data
  let contractBaseInfoArray = data.OptionContractBaseInfo.map((item) => {
    return {
      Species: item.COMMODITYID,
      'Contract Code': item.INSTRUMENTID,
      'Contract Size': item.TRADEUNIT,
      'Minimum Price Fluctuation': item.PRICETICK,
      'First Trading Day': item.OPENDATE,
      'Last Trading Day': item.EXPIREDATE
    };
  });

  const s3Params = {
    Bucket: process.env.AWS_S3_BUCKET,
    Key: `${dir}/SHFE-Contract-Parameters-Options-${dateStr}.csv`,
    Body: Papa.unparse(contractBaseInfoArray)
  };

  await s3.putObject(s3Params).promise();

  console.log(`Completed uploading to s3 contractParameters`);
};

const tradeParameters = async function (day, dir, dateStr) {
  console.log(`running tradeParameters`);

  const { data } = await axios.get(
    `http://www.shfe.com.cn/data/instrument/option/ContractDailyTradeArgument${day}.dat`
  );

  //Choose the required data
  let contractDailyArgumentArray = data.OptionContractDailyTradeArgument.map((item) => {
    return {
      'Contract Code': item.INSTRUMENTID,
      'Speculative Trading Margin': item.STRADEUNITMARGIN,
      'Hedging Trading Margin': item.HTRADEUNITMARGIN,
      'Limit-Up Price': item.UPPERVALUE,
      'Limit-Down Price': item.LOWERVALUE
    };
  });

  const s3Params = {
    Bucket: process.env.AWS_S3_BUCKET,
    Key: `${dir}/SHFE-Trading-Parameters-Options-${dateStr}.csv`,
    Body: Papa.unparse(contractDailyArgumentArray)
  };

  await s3.putObject(s3Params).promise();

  console.log(`Completed uploading to s3 tradeParameters`);
};

const settlementParameters = async function (day, dir, dateStr) {
  console.log(`running settlementParameters`);

  const { data } = await axios(`http://www.shfe.com.cn/data/instrument/option/Settlement${day}.dat`);

  // const note =
  //   'Note: 1. The settlement parameters table will be released after end-of-day clearing and settlement. 2. Unit for options settlement price: RMB/metric ton for copper and Natural Rubber. RMB yuan/g for Gold. 3. SP : Settlement Price. 4. TTFR(‰) : Trade Transaction Fee Rate(‰). 5. TTF(R/L) : Trade Transaction Fee(RMB/lot). 6. E/S TFR(‰) : Exercise/Settle Transaction Fee Rate(‰). 7. E/S TF(R/L) : Exercise/Settle Transaction Fee(RMB/lot). 8. STM(R/L) : Speculative Trading Margin(RMB/lot). 9. HTM(R/L) : Hedging Trading Margin(RMB/lot). 10. OOTFR(‰) : Option Offset Transaction Fee Rate(‰). 11. OOTF(R/L) : Option Offset Transaction Fee(RMB/lot). 12. OSTFR(‰) : Option Settlement Transaction Fee Rate(‰). 13. OSTF(R/L) : Option Settlement Transaction Fee(RMB/lot). 14. DRFCTP : Discount Rate for Closing-out Today’s Position.';

  //Choose the required data
  let settlementArray = data.OptionSettlement.map((item) => {
    return {
      Species: item.INSTRUMENTID.substr(0, 2),
      'Contract Code': item.INSTRUMENTID,
      SP: item.SETTLEMENTPRICE,
      'TTFR(%)': item.TRADEFEERATIO,
      'TTF (R/L)': item.TRADEFEEUNIT,
      'E/S TFR(%)': item.STRIKEFEERATIO,
      'E/S TF (R/L)': item.STRIKEFEEUNIT,
      'STM (R/L)': item.STRADEUNITMARGIN,
      'HTM (R/L)': item.HTRADEUNITMARGIN,
      'OOT FR(%)': item.STRIKEFEERATIO,
      'OOTF (R/L)': item.STRIKEFEEUNIT,
      'OST FR(%)': item.STRIKECLOSEFEERATIO,
      'OSTF (R/L)': item.STRIKECLOSEFEEUNIT,
      'DRFC TP(%)': item.DISCOUNTRATE
    };
  });

  const s3Params = {
    Bucket: process.env.AWS_S3_BUCKET,
    Key: `${dir}/SHFE-Settlement-Parameters-Options-${dateStr}.csv`,
    Body: Papa.unparse(settlementArray) //+ '\n' + note
  };

  await s3.putObject(s3Params).promise();

  console.log(`Completed uploading to s3 settlementParameters`);
};

const getOptionsData = async function (day, dir, dateStr) {
  await dailyData(day, dir, dateStr);
  await contractParameters(day, dir, dateStr);
  await tradeParameters(day, dir, dateStr);
  await settlementParameters(day, dir, dateStr);
};

exports.getOptionsData = getOptionsData;
