const Papa = require('papaparse');
const fs = require('fs');
const axios = require('axios');

// aws-sdk is always preinstalled in AWS Lambda in all Node.js runtimes
const AWS = require('aws-sdk');
const s3 = new AWS.S3({ region: 'us-east-2' });

const getAggregateReferenceDataOptions = async (day, dir, dateStr) => {
  let endpoints = [
    `http://www.shfe.com.cn/data/instrument/option/ContractBaseInfo${day}.dat`,
    `http://www.shfe.com.cn/data/instrument/option/ContractDailyTradeArgument${day}.dat`,
    `http://www.shfe.com.cn/data/instrument/option/Settlement${day}.dat`
  ];

  let array = [];

  const data = await axios.all(endpoints.map((endpoint) => axios.get(endpoint))).then(
    axios.spread(({ data: contractParam }, { data: tradeParam }, { data: settlementParam }) => {
      contractParam.OptionContractBaseInfo.map((item) => {
        array.push({
          Species: item.COMMODITYID,
          'Contract Code': item.INSTRUMENTID,
          'Contract Size': item.TRADEUNIT,
          'Minimum Price Fluctuation': item.PRICETICK,
          'First Trading Day': item.OPENDATE,
          'Last Trading Day': item.EXPIREDATE
        });
      });
      tradeParam.OptionContractDailyTradeArgument.map((item) => {
        array.map((val) => {
          if (item.INSTRUMENTID === val['Contract Code']) {
            (val['Speculative Trading Margin'] = item.STRADEUNITMARGIN),
              (val['Hedging Trading Margin'] = item.HTRADEUNITMARGIN),
              (val['Limit-Up Price'] = item.UPPERVALUE),
              (val['Limit-Down Price'] = item.LOWERVALUE);
          }
        });
      });
      settlementParam.OptionSettlement.map((item) => {
        array.map((val) => {
          if (item.INSTRUMENTID === val['Contract Code']) {
            (val.SP = item.SETTLEMENTPRICE),
              (val['TTFR(%)'] = item.TRADEFEERATIO),
              (val['TTF (R/L)'] = item.TRADEFEEUNIT),
              (val['E/S TFR(%)'] = item.STRIKEFEERATIO),
              (val['E/S TF (R/L)'] = item.STRIKEFEEUNIT),
              (val['STM (R/L)'] = item.STRADEUNITMARGIN),
              (val['HTM (R/L)'] = item.HTRADEUNITMARGIN),
              (val['OOT FR(%)'] = item.STRIKEFEERATIO),
              (val['OOTF (R/L)'] = item.STRIKEFEEUNIT),
              (val['OST FR(%)'] = item.STRIKECLOSEFEERATIO),
              (val['OSTF (R/L)'] = item.STRIKECLOSEFEEUNIT),
              (val['DRFC TP(%)'] = item.DISCOUNTRATE);
          }
        });
      });
    })
  );
  const s3Params = {
    Bucket: process.env.AWS_S3_BUCKET,
    Key: `${dir}/SHFE-Aggregate-Reference-Data/SHFE-Aggregate-Reference-Data-Options-${dateStr}.csv`,
    Body: Papa.unparse(array)
  };

  await s3.putObject(s3Params).promise();
};

exports.getAggregateReferenceDataOptions = getAggregateReferenceDataOptions;
