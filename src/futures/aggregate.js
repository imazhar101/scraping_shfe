'use strict';

const Papa = require('papaparse');
const fs = require('fs');
const axios = require('axios');

// aws-sdk is always preinstalled in AWS Lambda in all Node.js runtimes
const AWS = require('aws-sdk');
const s3 = new AWS.S3({ region: 'us-east-2' });

//Calculate delivery fee for settlement parameters and delivery parameters
function get_delivery_fee(item) {
  const setp = Number(item.COMMODITYDELIVERYFEEUNIT);
  const productid = item.INSTRUMENTID;
  let price;
  if (productid.indexOf('cu') >= 0) {
    price = (setp / 5).toFixed(0);
  } else if (productid.indexOf('al') >= 0) {
    price = (setp / 5).toFixed(0);
  } else if (productid.indexOf('zn') >= 0) {
    price = (setp / 5).toFixed(0);
  } else if (productid.indexOf('pb') >= 0) {
    price = (setp / 5).toFixed(0);
  } else if (productid.indexOf('ni') >= 0) {
    price = (setp / 1).toFixed(0);
  } else if (productid.indexOf('sn') >= 0) {
    price = (setp / 1).toFixed(0);
  } else if (productid.indexOf('au') >= 0) {
    price = (setp / 1000).toFixed(2);
  } else if (productid.indexOf('ag') >= 0) {
    price = (setp / 15).toFixed(2);
  } else if (productid.indexOf('rb') >= 0) {
    price = (setp / 10).toFixed(0);
  } else if (productid.indexOf('wr') >= 0) {
    price = (setp / 10).toFixed(0);
  } else if (productid.indexOf('hc') >= 0) {
    price = (setp / 10).toFixed(0);
  } else if (productid.indexOf('fu') >= 0) {
    price = (setp / 10).toFixed(0);
  } else if (productid.indexOf('bu') >= 0) {
    price = (setp / 10).toFixed(0);
  } else if (productid.indexOf('ru') >= 0) {
    price = (setp / 10).toFixed(0);
  } else if (productid.indexOf('sc') >= 0) {
    price = (setp / 1000).toFixed(2);
  } else if (productid.indexOf('sp') >= 0) {
    price = (setp / 10).toFixed(0);
  } else if (productid.indexOf('nr') >= 0) {
    price = (setp / 10).toFixed(0);
  } else if (productid.indexOf('ss') >= 0) {
    price = (setp / 5).toFixed(0);
  }

  return price;
}

const getAggregateReferenceDataFutures = async (day, dir, dateStr) => {
  let endpoints = [
    `http://www.shfe.com.cn/data/instrument/ContractBaseInfo${day}.dat`,
    `http://www.shfe.com.cn/data/instrument/ContractDailyTradeArgument${day}.dat`,
    `http://www.shfe.com.cn/data/instrument/Settlement${day}.dat`,
    `http://www.shfe.com.cn/data/instrument/Delivery${day}.dat`
  ];

  let array = [];

  const data = await axios.all(endpoints.map((endpoint) => axios.get(endpoint))).then(
    axios.spread(
      ({ data: contractParam }, { data: tradeParam }, { data: settlementParam }, { data: deliveryParam }) => {
        contractParam.ContractBaseInfo.map((item) => {
          array.push({
            'Contract Code': item.INSTRUMENTID.trim(),
            'Listing Date': item.OPENDATE,
            'Expiration Date': item.EXPIREDATE,
            'First Delivery Day': item.STARTDELIVDATE,
            'Last Delivery Day': item.ENDDELIVDATE,
            'Benchmark Price': item.BASISPRICE
          });
        });
        tradeParam.ContractDailyTradeArgument.map((item) => {
          array.map((val) => {
            if (item.INSTRUMENTID === val['Contract Code']) {
              (val['Margin Rate for Long Speculation (%)'] = item.SPEC_LONGMARGINRATIO * 100),
                (val['Margin Rate for Short Speculation (%)'] = item.SPEC_SHORTMARGINRATIO * 100),
                (val['Margin Rate for Long Hedging (%)'] = item.HDEGE_LONGMARGINRATIO * 100),
                (val['Margin Rate for Short Hedging (%)'] = item.HDEGE_SHORTMARGINRATIO * 100),
                (val['Limit Up(%)'] = item.UPPER_VALUE * 100),
                (val['Limit Down(%)'] = item.LOWER_VALUE * 100);
            }
          });
        });
        settlementParam.Settlement.map((item) => {
          array.map((val) => {
            if (item.INSTRUMENTID === val['Contract Code']) {
              (val.Settle = item.SETTLEMENTPRICE),
                (val['Transaction Fee(%)'] = item.TRADEFEERATION * 1000),
                (val['Transaction Fee Amount (RMB/ Contract)'] = item.TRADEFEEUNIT),
                (val['Delivery Fee'] = get_delivery_fee(item) ? get_delivery_fee(item) : 0), //need to modify
                (val['Margin Rate for Long Speculation (%)'] = item.SPEC_LONGMARGINRATIO * 100),
                (val['Margin Rate for Short Speculation (%)'] = item.SPEC_SHORTMARGINRATIO * 100),
                (val['Margin Rate for Long Hedging (%)'] = item.LONGMARGINRATIO * 100),
                (val['Margin Rate for Short Hedging (%)'] = item.SHORTMARGINRATIO * 100),
                (val["Discount Rate for Closing-out Today's Position (%)"] = item.DISCOUNTRATE * 100);
            }
          });
        });
        deliveryParam.Delivery.map((item) => {
          array.map((val) => {
            if (item.INSTRUMENTID === val['Contract Code']) {
              (val['Tax-paid Delivery and Settlement Price'] = item.DELIVERYPRICE === '' ? 0 : item.DELIVERYPRICE),
                (val['Bonded Delivery and Settlement Price'] =
                  item.BONDEDDELIVERYPRICE === '' ? 0 : item.BONDEDDELIVERYPRICE),
                (val['Delivery Margin Rate(%)'] = item.LONGMARGINRATIO * 100),
                (val['Delivery Fee'] = get_delivery_fee(item) ? get_delivery_fee(item) : 0);
            }
          });
        });
      }
    )
  );
  const s3Params = {
    Bucket: process.env.AWS_S3_BUCKET,
    Key: `${dir}/SHFE-Aggregate-Reference-Data/SHFE-Aggregate-Reference-Data-Futures-${dateStr}.csv`,
    Body: Papa.unparse(array) 
  };

  await s3.putObject(s3Params).promise();
};

exports.getAggregateReferenceDataFutures = getAggregateReferenceDataFutures;
