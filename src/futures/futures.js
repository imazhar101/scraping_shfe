'use strict';

require('dotenv').config();

const Papa = require('papaparse');
const axios = require('axios');

// aws-sdk is always preinstalled in AWS Lambda in all Node.js runtimes
const AWS = require('aws-sdk');
const s3 = new AWS.S3({ region: 'us-east-2' });

// Calculate delivery fee for settlement parameters and delivery parameters
function getDeliveryFee(item) {
  const setp = Number(item.COMMODITYDELIVERYFEEUNIT);
  const productid = item.INSTRUMENTID;

  let price;
  if (productid.indexOf('cu') >= 0) {
    price = (setp / 5).toFixed(0);
  } else if (productid.indexOf('al') >= 0) {
    price = (setp / 5).toFixed(0);
  } else if (productid.indexOf('zn') >= 0) {
    price = (setp / 5).toFixed(0);
  } else if (productid.indexOf('pb') >= 0) {
    price = (setp / 5).toFixed(0);
  } else if (productid.indexOf('ni') >= 0) {
    price = (setp / 1).toFixed(0);
  } else if (productid.indexOf('sn') >= 0) {
    price = (setp / 1).toFixed(0);
  } else if (productid.indexOf('au') >= 0) {
    price = (setp / 1000).toFixed(2);
  } else if (productid.indexOf('ag') >= 0) {
    price = (setp / 15).toFixed(2);
  } else if (productid.indexOf('rb') >= 0) {
    price = (setp / 10).toFixed(0);
  } else if (productid.indexOf('wr') >= 0) {
    price = (setp / 10).toFixed(0);
  } else if (productid.indexOf('hc') >= 0) {
    price = (setp / 10).toFixed(0);
  } else if (productid.indexOf('fu') >= 0) {
    price = (setp / 10).toFixed(0);
  } else if (productid.indexOf('bu') >= 0) {
    price = (setp / 10).toFixed(0);
  } else if (productid.indexOf('ru') >= 0) {
    price = (setp / 10).toFixed(0);
  } else if (productid.indexOf('sc') >= 0) {
    price = (setp / 1000).toFixed(2);
  } else if (productid.indexOf('sp') >= 0) {
    price = (setp / 10).toFixed(0);
  } else if (productid.indexOf('nr') >= 0) {
    price = (setp / 10).toFixed(0);
  } else if (productid.indexOf('ss') >= 0) {
    price = (setp / 5).toFixed(0);
  }

  return price;
}

const dailyData = async function (day,dir, dateStr) {
  console.log(`running dailyData`);

  const { data } = await axios(
    `http://www.shfe.com.cn/data/dailydata/kx/kx${day}.dat` // url contains date in YYYYMMDD format
  );

  //regex to remove chinese expressions present in data
  const regex1 = new RegExp(/小计/);
  const regex2 = new RegExp(/合计/);
  const regex3 = new RegExp(/总计/);
  const regex4 = new RegExp(/鎬昏/);

  //additional notes present in website for futures EOD pricing data
  // const note1 =
  //   'Note： 1. Quoted Price: RMB yuan/ton for Copper， Aluminum， Zinc， Nickel，Stannum，Rebar， Wire-rod，Hot rolled coils， Lead， Natural Rubber， Fuel Oil， Bitumen， Woodpulp， Stainless Steel. RMB yuan/g for Gold. RMB yuan/ kg for Sliver.RMB yuan/barrel for Crude Oil (the quotation is tax-exclusive price).RMB yuan/ton for LSFO， TSR 20 and Copper(BC) (the quotation is tax-exclusive price). 2. Unit Price: 5 tons/lot for Copper/Copper(BC)/Aluminum/Zinc/Stainless Steel; 1 tons/lot for Nickel/Stannum; 5 tons/lot for Natural Rubber(adjusted to 10tons since ru1208); 5tons/lot for Lead， 10tons/lot for Rebar， Wire-rod， LSFO， Hot rolled coils， Bitumen， TSR 20 and Woodpulp; 10tons/lot for fuel oil(adjusted to 10 tons/lot since fu1202);1000grams/lot for Gold; 15kilograms/lot for Sliver.1000bbl/lot for Crude Oil. 3. The unit of trading volume， open interest and change of positions is lot， which is counted as one-sided. The unit of turnover is RMB 10，000， which is counted as one-sided. 4. price change 1=closing price-settlement price of previous day ; price change 2=settlement price - settlement price of previous day. 5. Futures trading volume and turnover including that of self-trades. 6.Before the settlement of the contract， its volume or turnover does not include that from TAS; After the settlement of the contract has finished， the TAS volume or turnover will be counted into the volume or turnover of the contract.';
  // const note2 =
  //   'Note： 1. The trading lots， turnover， annual trading lots and turnover are counted as one-sided. 2. Futures trading volume and turnover including that of self-trades. 3. Before the settlement of the contract， its volume or turnover does not include that from TAS; After the settlement of the contract has finished， the TAS volume or turnover will be counted into the volume or turnover of the contract.\nSHFE Price index 2022-01-28';
  // const note3 =
  //   "Note： Price change 1=closing price- reference settlement price of previous day ; price change 2= reference settlement price - reference settlement price of previous day.  Index Disclaimer: 1. All information in this article， including but not limited to all text， data， graph， table and formula (hereinafter referred to as 'Information')， is the property of the Shanghai Futures Exchange (SHFE)， which is protected by the China Intellectual Property Law and other relevant laws. The Information is for reference only. 2. All Information in this article， of which the sources are recognized as reliable， is only for informative use， rather than provided for specific individuals， companies or other organizations. Any Information in this article， or other content that contains the above Information， does not constitute any basis or advice of SHFE for any futures， other financial products or investment instruments， or any trading strategies. SHFE and its related parties do not recognize， consent or in other ways give any advice on any futures company， futures， other financial products or investment instruments， or any trading strategies. Investors making investment decisions should fully take into account the investment risks， and understand all investment-related procedures and documents involved， rather than the statements in this article. 3. SHFE does not guarantee the accuracy and completeness of all Information. Neither will it assume any responsibility resulting from calculation error， omission or publishing interruption in the preparation and publishing of all Information. It neither expresses or implies the use of the Information in this article， nor guarantees the suitability of use of the Information. It will not bear any legal responsibility for any claims or damages， including but not limited to loss of interest， punitive or indirect damages as a result of the inaccurate or incomplete content， arising directly or indirectly from use of any Information in this article when the Information is considered to be applicable.  Shanghai Futures Exchange Ownership Reservation";

  //get o_curinstrument data from API
  const currentInstrumentArray = data.o_curinstrument.map((item) => {
    return {
      'Product ID': regex3.test(item.PRODUCTID.trim()) ? 'Total:' : item.PRODUCTID.trim(),
      'Product Group ID':
        item.PRODUCTGROUPID.trim() === 'sc_tas' ? 'sc_tas' : item.CLOSEPRICE ? item.PRODUCTGROUPID.trim() : '',
      'Delivery month':
        regex1.test(item.DELIVERYMONTH) || regex2.test(item.DELIVERYMONTH) ? 'Total:' : item.DELIVERYMONTH,
      'Pre settle': item.PRESETTLEMENTPRICE,
      Open: item.OPENPRICE,
      High: item.HIGHESTPRICE,
      Low: item.LOWESTPRICE,
      Close: item.CLOSEPRICE,
      Settle: item.SETTLEMENTPRICE,
      ch1: item.ZD1_CHG,
      ch2: item.ZD2_CHG,
      Volume: item.VOLUME === '' ? 0 : item.VOLUME,
      Turnover: item.TURNOVER,
      'Open Interest': item.OPENINTEREST,
      'Open Interest Change': item.OPENINTERESTCHG
    };
  });

  //get o_curproduct data from API
  const currentProductArray = data.o_curproduct.map((item) => {
    return {
      Species: regex3.test(item.PRODUCTID) || regex4.test(item.PRODUCTID) ? 'Total:' : item.PRODUCTID.trim(),
      High: item.HIGHESTPRICE,
      Low: item.LOWESTPRICE,
      'Weighted Average Price': item.AVGPRICE,
      'trading volume(lot)': item.VOLUME,
      'Turnover(hundred million yuan)': item.TURNOVER,
      'annual trading volume(ten thousand lots)': item.YEARVOLUME,
      'annual Turnover(hundred million yuan)': item.YEARTURNOVER
    };
  });

  //get o_curmetalindex data from API

  const currentMetalIndexArray = data.o_curmetalindex.map((item) => {
    return {
      'Name of Index': 'SHFE Nonferrous Metals Index',
      Last: item.LASTPRICE,
      Open: item.OPENPRICE,
      High: item.HIGHESTPRICE,
      Low: item.LOWESTPRICE,
      Close: item.CLOSEPRICE,
      'Last Close': item.PRECLOSEPRICE,
      ch1: item.UPDOWN1,
      ch2: item.UPDOWN2,
      'Clearing price for reference': item.SETTLEMENTPRICE
    };
  });

  const removeByAttr = function (arr, attr, value) {
    var i = arr.length;
    while (i--) {
      if (arr[i] && arr[i].hasOwnProperty(attr) && arguments.length > 2 && arr[i][attr] === value) {
        arr.splice(i, 1);
      }
    }
    return arr;
  };

  removeByAttr(currentInstrumentArray, 'Product ID', 'Total:');
  removeByAttr(currentInstrumentArray, 'Delivery month', 'Total:');
  removeByAttr(currentProductArray, 'Species', 'Total:');

  const s3Params = {
    Bucket: process.env.AWS_S3_BUCKET,
    Key: `${dir}/SHFE-EODPrice-Futures-${dateStr}.csv`,
    Body:
      Papa.unparse(currentInstrumentArray) +
      // '\n' +
      // note1 +
      '\n\n' +
      Papa.unparse(currentProductArray) +
      // '\n' +
      // note2 +
      '\n\n' +
      Papa.unparse(currentMetalIndexArray) +
      // '\n' +
      // note3 +
      '\n\n'
  };

  await s3.putObject(s3Params).promise();

  console.log(`Completed uploading to s3 dailyData`);
};

const contractParameters = async function (day,dir,dateStr) {
  console.log(`running contractParameters`);

  const { data } = await axios.get(`http://www.shfe.com.cn/data/instrument/ContractBaseInfo${day}.dat`);

  // Choose the required data
  let contractBaseInfoArray = data.ContractBaseInfo.map((item) => {
    return {
      'Contract Code': item.INSTRUMENTID.trim(),
      'Listing Date': item.OPENDATE,
      'Expiration Date': item.EXPIREDATE,
      'First Delivery Day': item.STARTDELIVDATE,
      'Last Delivery Day': item.ENDDELIVDATE,
      'Benchmark Price': item.BASISPRICE
    };
  });

  const s3Params = {
    Bucket: process.env.AWS_S3_BUCKET,
    Key: `${dir}/SHFE-Contract-Parameters-Futures-${dateStr}.csv`,
    Body: Papa.unparse(contractBaseInfoArray)
  };

  await s3.putObject(s3Params).promise();

  console.log(`Completed uploading to s3 contractParameters`);
};

const tradeParameters = async function (day,dir,dateStr) {
  console.log(`running tradeParameters`);

  const { data } = await axios.get(`http://www.shfe.com.cn/data/instrument/ContractDailyTradeArgument${day}.dat`);

  //Choose the required data
  let contractDailyArgumentArray = data.ContractDailyTradeArgument.map((item) => {
    return {
      'Contract Code': item.INSTRUMENTID,
      'Margin Rate for Long Speculation (%)': item.SPEC_LONGMARGINRATIO * 100,
      'Margin Rate for Short Speculation (%)': item.SPEC_SHORTMARGINRATIO * 100,
      'Margin Rate for Long Hedging (%)': item.HDEGE_LONGMARGINRATIO * 100,
      'Margin Rate for Short Hedging (%)': item.HDEGE_SHORTMARGINRATIO * 100,
      'Limit Up(%)': item.UPPER_VALUE * 100,
      'Limit Down(%)': item.LOWER_VALUE * 100
    };
  });
  const s3Params = {
    Bucket: process.env.AWS_S3_BUCKET,
    Key: `${dir}/SHFE-Trading-Parameters-Futures-${dateStr}.csv`,
    Body: Papa.unparse(contractDailyArgumentArray)
  };

  await s3.putObject(s3Params).promise();

  console.log(`Completed uploading to s3 tradeParameters`);
};

const settlementParameters = async function (day,dir,dateStr) {
  console.log(`running settlementParameters`);

  const { data } = await axios(`http://www.shfe.com.cn/data/instrument/Settlement${day}.dat`);

  // const note =
  //   'Note: 1. This settlement parameter statement is released after the completion of settlement on the very day. 2. The delivery fee quotation unit for copper， aluminum， zinc， lead， nickel，Stannum，rebar， wire rod，Hot rolled coils， natural rubber， fuel oil， petroleum asphalt， TSR 20， Woodpulp， Stainless Steel is RMB/ton; that for gold is RMB/gram; and that for silver is RMB/kg; and that for crude oil is RMB/barrel. 3.The transaction fee for closing out the position opened on the same day=the haircut rate for closing out the position opened on the same day * transaction fee， which is waived then the value is zero. 4.Copper， lead， silver， steel rebar， wire rod， hot-rolled coil， fuel oil， bitumen， natural rubber， Woodpulp， Stainless Steel: transaction fee these futures shall refer to the rate of transaction fee. 5.Aluminum， zinc， nickel， tin， gold， crude oil， TSR 20: transaction fee for these futures shall refer to the amount of transaction fee.';

  //Choose the required data
  let settlementArray = data.Settlement.map((item) => {
    return {
      'Contract Code': item.INSTRUMENTID,
      Settle: item.SETTLEMENTPRICE,
      'Transaction Fee(%)': item.TRADEFEERATION * 1000,
      'Transaction Fee Amount (RMB/ Contract)': item.TRADEFEEUNIT,
      'Delivery Fee': getDeliveryFee(item) ? getDeliveryFee(item) : 0, //need to modify
      'Margin Rate for Long Speculation (%)': item.SPEC_LONGMARGINRATIO * 100,
      'Margin Rate for Short Speculation (%)': item.SPEC_SHORTMARGINRATIO * 100,
      'Margin Rate for Long Hedging (%)': item.LONGMARGINRATIO * 100,
      'Margin Rate for Short Hedging (%)': item.SHORTMARGINRATIO * 100,
      "Discount Rate for Closing-out Today's Position (%)": item.DISCOUNTRATE * 100
    };
  });

  const s3Params = {
    Bucket: process.env.AWS_S3_BUCKET,
    Key: `${dir}/SHFE-Settlement-Parameters-Futures-${dateStr}.csv`,
    Body: Papa.unparse(settlementArray)  // + '\n' + note
  };

  await s3.putObject(s3Params).promise();

  console.log(`Completed uploading to s3 settlementParameters`);
};

const deliveryParameters = async function (day,dir,dateStr) {
  console.log(`running deliveryParameters`);

  const { data } = await axios(`http://www.shfe.com.cn/data/instrument/Delivery${day}.dat`);

  // const note =
  //   'Note: 1. The delivery fee quotation unit for copper， aluminum， zinc， lead， nickel，Stannum，rebar， wire rod， Hot rolled coils，natural rubber， fuel oil， petroleum asphalt， TSR 20， Woodpulp， Stainless Steel is RMB/ton; that for gold is RMB/gram; that for silver is RMB/kg; and that for crude oil is RMB/barrel.';

  const deliveryArray = data.Delivery.map((item) => {
    return {
      'Contract Code': item.INSTRUMENTID,
      'Tax-paid Delivery and Settlement Price': item.DELIVERYPRICE === '' ? 0 : item.DELIVERYPRICE,
      'Bonded Delivery and Settlement Price': item.BONDEDDELIVERYPRICE === '' ? 0 : item.BONDEDDELIVERYPRICE,
      'Delivery Margin Rate(%)': item.LONGMARGINRATIO * 100,
      'Delivery Fee': getDeliveryFee(item) ? getDeliveryFee(item) : 0,
      'Delivery Start Date': item.STARTDELIVERYDATE,
      'Delivery End Date': item.ENDDELIVERYDATE
    };
  });
  const s3Params = {
    Bucket: process.env.AWS_S3_BUCKET,
    Key: `${dir}/SHFE-Delivery-Parameters-Futures-${dateStr}.csv`,
    Body: Papa.unparse(deliveryArray)  //+ '\n' + note
  };

  await s3.putObject(s3Params).promise();

  console.log(`Completed uploading to s3 deliveryParameters`);
};

const getFuturesData = async function (day,dir,dateStr) {
  await dailyData(day,dir,dateStr);
  await contractParameters(day,dir,dateStr);
  await tradeParameters(day,dir,dateStr);
  await settlementParameters(day,dir,dateStr);
  await deliveryParameters(day,dir,dateStr);
};

exports.getFuturesData = getFuturesData;
